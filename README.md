**Study concerning the weight of 189 newborn depending on different parameters during the pregnancy**

- Data available in the MASS package in R (189 rows and 10 columns)

**Step 1**
Load and describe the file.

**Step 2**
Main features of the data.

**Step 3**
Descriptive statistics about the ht variable (hypertension).

**Step 4**
Probabilities.

**Step 5**
Conditional probabilities.

**Step 6**
Recap tabs.


Author Marion Estoup

E-mail : marion_110@hotmail.fr

January 2021
